/**
 * Bài 1: Tính tiền lương nhân viên với 1 ngày lương là 100.000
 * Input: Số ngày làm 22 ngày
 *
 * Step: 1- Tạo biến lưu trữ số tiền lương 1 ngày là 100000
 *       2- Tạo biến lưu trữ do người dùng nhập vào số ngày làm việc (vd 22 ngày) (Chưa biết tạo cho người dùng nhập vào hi)
 *       3- Tạo biến lưu trữ tổng số tiền lương totalSalary = oneDaySalary * day
 *       4- Trả kết quả ra tab console
 * Output: Số tiền lương tháng này là: 2200000 Vnđ
 */
var oneDaySalary = 100000;
var day = 22;
var totalSalary = 0;
totalSalary = oneDaySalary * day;
console.log("Bài 1: Số tiền lương tháng này là:", totalSalary, "Vnđ");
/**
 * Bài 2: Tính giá trị trung bình của 5 số thực và xuất ra màn hình
 * Input: Nhập 5 số thực num1, num2, num3, num4, num5
 *
 * Step: 1- Tạo biến cho người dùng nhập vào 5 số lần lượt là num1 = 4, num2 = 7, num3 = 5, num4 = 9, num5 = 10
 *       2- Tạo biến lưu giá trị trung bình của 5 số bằng tổng 5 số chia cho 5
 *       3- Xuất kết quả ra màn hình
 * Output: Giá trị trung bình là:
 */
var num1 = 4,
  num2 = 7,
  num3 = 5,
  num4 = 9,
  num5 = 10;
var average = 0;
average = (num1 + num2 + num3 + num4 + num5) / 5;
document.write("Bài 2: Giá trị trung bình là: ", average);
/**
 * Bài 3: Quy đổi sang từ USD sang VND với giá 1 USD = 23.500 VND
 * Input: Nhập số USD cần quy đổi  (VD 2 USD)
 *
 * Step: 1- Tạo biến lưu trữ tỷ giá quy đổi tiền USD sang VND
 *       2- Tạo biến lưu trữ số USD do người dùng nhập vào (vd 2 USD)
 *       3- Tạo biến lưu trữ kết quả do sau quy đổi vnd = usd * oneUsdtoVnd
 *       4- Trả kết quả trong tab console
 * Output: Bạn có thể đổi thành: 47000 VND
 */
var oneUsdtoVnd = 23500;
var usd = 2;
var vnd = 0;
vnd = usd * oneUsdtoVnd;
console.log("Bài 3: Bạn có thể đổi thành: ", vnd, "VND");
/**Bài 4: Tính diện tích và chu vi hình chữ nhật khi nhập vào chiều dài và chiều rộng
 * Input: 1- Nhập vào chiều dài được nhập vào longOfRec (vd 5 m)
 *        2- Nhập vào chiều rộng widthOfRec (vd 3)
 *
 * Step:1- Tạo biến lưu trữ chiều dài được nhập vào longOfRec (vd 5 m)
 *        2- Tạo biến lưu trữ chiều rộng widthOfRec (vd 3)
 *        3- Tạo biến lưu kết quả diện tích area = longOfRec * widthOfRec
 *        4- Tạo biến lưu kết quả chu vi perimeter = (longOfRec + widthOfRec)*2
 *
 * Output: 1- Diện tích hình chữ nhật là: 15 m2
 *         2- Chu vi hình chữ nhật là: 16 m
 */
var longOfRec = 5,
  widthOfRec = 3;
var area = 0;
area = longOfRec * widthOfRec;
var perimeter = 0;
perimeter = (longOfRec + widthOfRec) * 2;
console.log("Bài 4: 1- Diện tích hình chữ nhật là: ", area, "m2");
console.log("Bài 4: 2- Chu vi hình chữ nhật là:", perimeter, "m");
/**Bài 5: Tính tổng 2 ký số
 * Input: Nhập vào 1 số có 2 chữ số (vd 44)
 *
 * Step: 1- Tạo biến lưu trữ số liệu được nhập vào number (vd 44)
 *       2- Tách số liệu và kưu vào biến được tạo là unit và ten
 *       3- Tạo biến lưu giá trị của tổng các ký số sum = ten + unit
 *
 * Output: Tổng 2 ký số là : 8
 */
var number = 44;
var unit = number % 10;
var ten = Math.floor(number / 10) % 10;
var sum = 0;
sum = ten + unit;
console.log("Bài 5: Tổng 2 ký số là : ", sum);
